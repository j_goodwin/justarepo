# ReadMe for JustARepo

## Simple Git repo to demonstrate loading of git repos via NPM

## Git instructions

1. Create a new project
   1. Create new local directory
   2. Run `npm init --scope=@L3Harris` and set suitable vales
      (`--scope` enables grouping of dependencies)
2. Add an external source

   ```bash
   npm i git@bitbucket.org:j_goodwin/justarepo.git
   ```

   via SSH or to execute via HTTPS:
  
   ```bash
   npm i git+https://j_goodwin@bitbucket.org/j_goodwin/justarepo.git
   ```

### Best Practise

Reference Git tags or commits from the repo to enable 'stable' code; reference with `#TAG_NAME` - e.g.

```bash
npm i git+https://j_goodwin@bitbucket.org/j_goodwin/justarepo.git#v1.0.0
```
